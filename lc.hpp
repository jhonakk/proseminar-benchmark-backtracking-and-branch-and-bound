#ifndef KNAPSACK_LC_HPP
#define KNAPSACK_LC_HPP

#include "helper_class.hpp"

class lc : public helper_class {

public:
    // constructor
    lc(float capa, const std::vector<t_obj>& objects);
    lc();
    ~lc();

    // algorithm
    void run_algorithm();
};

#endif //KNAPSACK_LC_HPP
