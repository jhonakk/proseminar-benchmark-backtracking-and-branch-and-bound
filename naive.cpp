#include <cmath>
#include "naive.hpp"

naive::naive(float capa, const std::vector<t_obj>& objects) {
    capa_ = capa;
    objects_ = objects;
    opt_sol_ = {};
    opt_win_ = 0;
    branch_amount_ = pow(2, objects_.size() + 1 ) - 2;
    branch_count_ = branch_amount_;
}

naive::naive() {
    opt_sol_ = {};
    opt_win_ = 0;
    generate_random_config(DEFAULT_MIN, DEFAULT_MAX);
}

naive::~naive() = default;

void naive::run_algorithm() {
    branch_count_ = branch_amount_;

    std::vector<bool> choices;
    algo_naive(0, choices);
}

void naive::algo_naive(unsigned int depth, std::vector<bool> choices) {
    if (depth < objects_.size()) {

        choices.push_back(false);
        algo_naive(depth+1, choices);

        choices.pop_back();
        choices.push_back(true);
        algo_naive(depth+1, choices);
    } else {
        std::vector<t_cho> p;

        for (unsigned int j = 0; j < choices.size(); j++) {
            t_cho current_choice{};
            current_choice.cho_obj_nr = j;
            current_choice.cho_chosen = true;
            current_choice.cho_factor = (float) choices.at(j);//
            p.push_back(current_choice);
        }

        if (weight(p) <= capa_ and value(p) > opt_win_) {
            opt_win_ = value(p);
            opt_sol_ = p;
        }
    }
}