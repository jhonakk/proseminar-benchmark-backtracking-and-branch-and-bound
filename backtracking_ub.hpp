#ifndef KNAPSACK_BACKTRACKING_UB_HPP
#define KNAPSACK_BACKTRACKING_UB_HPP

#include "helper_class.hpp"

class backtracking_ub : public helper_class {

public:
    // constructor
    backtracking_ub(float capa, const std::vector<t_obj>& objects);
    backtracking_ub();
    ~backtracking_ub();

    // algorithm
    void run_algorithm();
    void algo_backtracking_ub(unsigned int depth, std::vector<t_cho> choices, float value, float weight);
};

#endif //KNAPSACK_BACKTRACKING_UB_HPP
