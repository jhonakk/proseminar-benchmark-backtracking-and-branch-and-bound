#include <cmath>
#include <functional>
#include <iostream>
#include <map>
#include <memory>
#include <string>
#include <stdlib.h>
#include <thread>
#include <vector>

#include "helper_class.hpp"

#include "naive.hpp"
#include "backtracking.hpp"
#include "backtracking_ub.hpp"
#include "lifo.hpp"
#include "lc.hpp"

#include "statistics.hpp"

void print_percentage(int bc, int n) {
    float p = (1 - (bc/(float)(n))) * 100;
    std::cout << "branches traversed: " << bc << "/" << n << " - ignored " << p << "% of branches." << std::endl;
}

int main(int argc, char** argv) {
    srand(time(nullptr));
    bool random_objects = false;
    auto stat = std::make_shared<statistics>(50, 5);

    unsigned int min_amount = 4;
    unsigned int max_amount = 20;

    unsigned int runs = 10;			// set amount of runs for automated statistics

    //-------------------------------------------------------------------------

    std::cout << "This is a program to calculate an optimal knapsack filling for " <<
              "the 0-1-knapsack-problem" << std::endl;

    // INIT

    std::map<std::string, std::shared_ptr<helper_class>> algorithms;
    algorithms["NAIVE"] = std::make_shared<naive>();
    algorithms["BACKTRACKING"] = std::make_shared<backtracking>();
    algorithms["BACKTRACKING WITH UPPER BOUND"] = std::make_shared<backtracking_ub>();
    algorithms["BRANCH-AND-BOUND: LIFO"] = std::make_shared<lifo>();
    algorithms["BRANCH-AND-BOUND: LEAST COST"] = std::make_shared<lc>();

    for (unsigned int run = 0; run < runs; run++) {
        std::cout << "RUN " << run << std::endl;
        unsigned int algo_count = 0;
        for (std::pair<std::string, std::shared_ptr<helper_class>> a : algorithms) {
            //std::cout << "current algo: " << a.first << std::endl;
            a.second->generate_random_config(min_amount, max_amount);
            a.second->run_algorithm();
            //std::cout << "branch count: " << a.second->get_branch_count() << std::endl;
            //std::cout << "branch amount: " << a.second->get_branch_amount() << std::endl;
            stat->set_data(algo_count, static_cast<float>(static_cast<float>(a.second->get_branch_count()) / static_cast<float>(a.second->get_branch_amount())));
            a.second->reset_algo();
            algo_count++;
        }
    }

    unsigned int algo_count = 0;
    std::cout << "\nRESULTS:\n" << std::endl;
    for (std::pair<std::string, std::shared_ptr<helper_class>> a : algorithms) {
        // name of algorithm
        std::cout << a.first << ":" << std::endl;

        /*
        // data
        std::cout << "data: ";
        for (unsigned int i = 0; i < stat->get_data().at(algo_count).size() - 1; i++) {
            std::cout << stat->get_data().at(algo_count).at(i) << ", ";
        }
        std::cout << stat->get_data().at(algo_count).at(stat->get_data().at(algo_count).size() - 1) << std::endl;
        std::cout << "test" << std::endl;
        */

        // mean value
        std::cout << "arithmetic mean: " << stat->mean_value().at(algo_count) << std::endl;

        // variance
        std::cout << "variance: " << stat->variance().at(algo_count) << std::endl;

        algo_count++;
        std::cout << std::endl;
    }

    /*

    if (random_objects) {



    } else {

        // adding all objects to a vector
        t_obj one{};
        one.obj_nr = 0;
        one.obj_value = 400.0;
        one.obj_weight = 2.0;
        one.obj_ratio = 200.0/2;
        objects.push_back(one);

        t_obj two{};
        two.obj_nr = 1;
        two.obj_value = 690.0;
        two.obj_weight = 3.0;
        two.obj_ratio = 690.0/3;
        objects.push_back(two);

        t_obj thr{};
        thr.obj_nr = 2;
        thr.obj_value = 900.0;
        thr.obj_weight = 4.0;
        thr.obj_ratio = 900.0/4;
        objects.push_back(thr);

        t_obj fur{};
        fur.obj_nr = 3;
        fur.obj_value = 1020.0;
        fur.obj_weight = 4.0;
        fur.obj_ratio = 1020.0/4;
        objects.push_back(fur);

        capa = 10;
    }


    std::cout << std::endl;
    std::cout << "beginn of method benchmark...";
    std::cout << std::endl;
    std::cout << std::endl;

    int branch_count_;
    int n = std::pow(2, objects.size() + 1) - 2;

    //-------------------------------------------------------------------------

    std::cout << std::endl;
    std::cout << "NAIVE:" << std::endl;

    naive nav;
    //nav.generate_random_config(min_amount, max_amount);
    nav.algo_naive_wrapper();
    print_percentage(n, n);
    auto ret_str = helper_class::print_choices(nav.get_opt_sol());
    std::cout << "opt_sol_ for naive = " << ret_str << std::endl;

    //-------------------------------------------------------------------------

    std::cout << std::endl;
    std::cout << "BACKTRACKING:" << std::endl;

    backtracking bt(capa, objects);
    bt.algo_backtracking_wrapper();
    int bc = bt.get_branch_count();
    print_percentage(bc, n);
    ret_str = helper_class::print_choices(bt.get_opt_sol());
    std::cout << "opt_sol_ for backtracking = " << ret_str << std::endl;

    //-------------------------------------------------------------------------

    std::cout << std::endl;
    std::cout << "BACKTRACKING WITH UPPER BOUND:" << std::endl;

    backtracking_ub bt_ub(capa, objects);
    bt_ub.algo_backtracking_ub_wrapper();
    bc = bt_ub.get_branch_count();
    print_percentage(bc, n);
    ret_str = helper_class::print_choices(bt_ub.get_opt_sol());
    std::cout << "opt_sol_ for backtracking_ub = " << ret_str << std::endl;

    //-------------------------------------------------------------------------

    std::cout << std::endl;
    std::cout << "BRANCH-AND-BOUND: LIFO" << std::endl;

    lifo bblifo(capa, objects);
    bblifo.algo_lifo_wrapper();
    bc = bblifo.get_branch_count();
    print_percentage(bc, n);
    ret_str = helper_class::print_choices(bblifo.get_opt_sol());
    std::cout << "opt_sol_ for lifo = " << ret_str << std::endl;

    //-------------------------------------------------------------------------

    std::cout << std::endl;
    std::cout << "BRANCH-AND-BOUND: LEAST COST" << std::endl;

    lc bblc(capa, objects);
    bblc.algo_lc();
    bc = bblc.get_branch_count();
    print_percentage(bc, n);
    ret_str = helper_class::print_choices(bblc.get_opt_sol());
    std::cout << "opt_sol_ for lc = " << ret_str << std::endl;

    */
    return 0;
}