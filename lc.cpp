#include <cmath>
#include <queue>

#include "lc.hpp"

lc::lc(float capa, const std::vector<t_obj>& objects) {
    capa_ = capa;
    objects_ = objects;
    opt_sol_ = {};
    opt_win_ = 0;
    branch_count_ = 0;
    branch_amount_ = pow(2, objects_.size() + 1 ) - 2;
}

lc::lc() {
    opt_sol_ = {};
    opt_win_ = 0;
    branch_count_ = 0;
    branch_amount_ = pow(2, objects_.size() + 1 ) - 2;
    generate_random_config(DEFAULT_MIN, DEFAULT_MAX);
}

lc::~lc() = default;

void lc::run_algorithm() {

    // init queue with empty choices
    std::priority_queue<std::pair<float, std::vector<t_cho>>> q;
    std::vector<t_cho> choices;
    float ub = calc_ub(choices);
    q.push(std::make_pair(ub, choices));

    while (not q.empty()) {
        choices = q.top().second;
        q.pop();
        branch_count_++;

        if (choices.size() >= objects_.size()) {
            opt_sol_ = choices;
            return;
        } else {
            // generate zero-choice
            t_cho new_choice{};
            new_choice.cho_obj_nr = choices.size();
            new_choice.cho_chosen = true;
            new_choice.cho_factor = 0;
            choices.push_back(new_choice);

            // add zero-choice to queue
            ub = calc_ub(choices);
            q.push(std::make_pair(ub, choices));

            // undo zero-choice, redo one-choice
            choices.pop_back();
            new_choice.cho_factor = 1;
            choices.push_back(new_choice);

            if (weight(choices) <= capa_) {
                // add one-choice to queue
                ub = calc_ub(choices);
                q.push(std::make_pair(ub, choices));
            }
        }
    }
    // undo root node choice
    branch_count_--;
}