#include <algorithm>
#include <iostream>
#include <cmath>
#include <stdlib.h>
#include <string>

#include "helper_class.hpp"

// sort function for rest_objects
inline bool compare (const t_obj& t1, const t_obj& t2) {
    return (t1.obj_ratio > t2.obj_ratio);
}

float helper_class::value(const std::vector<struct t_cho>& choices) {
    float v = 0;
    for (const auto& c : choices) {
        v += c.cho_factor * objects_.at(c.cho_obj_nr).obj_value;
    }
    return v;
}

float helper_class::weight(const std::vector<struct t_cho>& choices) {
    float w = 0;
    for (const auto& c : choices) {
        w += c.cho_factor * objects_.at(c.cho_obj_nr).obj_weight;
    }
    return w;
}

float helper_class::calc_ub(const std::vector<struct t_cho>& choices) {
    // rest capacity
    float r_capa = capa_ - weight(choices);

    // rest objects is created by deleting objects that are already chosen
    std::vector<struct t_obj> r_obj = objects_;
    for (const auto& c : choices) {
        for (unsigned int o = 0; o < r_obj.size(); o++) {
            if (r_obj.at(o).obj_nr == c.cho_obj_nr) {
                r_obj.erase(r_obj.begin() + o);
                o--;
            }
        }
    }

    std::sort(r_obj.begin(), r_obj.end(), compare);

    // not necessary to explicitely generate rk_choices bc we're only interested in the values

    float v = 0;		// value
    float w = 0;		// weight
    float f = 0;		// factor

    for (unsigned int i = 0; i < r_obj.size(); i++) {
        if (w < r_capa) {
            if (w + r_obj.at(i).obj_weight <= r_capa) {
                // take object entirely
                f = 1;
            } else {
                // take object partially
                f = (r_capa - w) / r_obj.at(i).obj_weight;
            }
            w += f * r_obj.at(i).obj_weight;
            v += f * r_obj.at(i).obj_value;
        }
    }

    return value(choices) + v;
}

void helper_class::init_vector_of_choices(std::vector<struct t_cho>& choices, const std::vector<struct t_obj>& objects) {
    for (auto o : objects) {
        t_cho new_choice{};
        new_choice.cho_obj_nr = o.obj_nr;
        choices.push_back(new_choice);
    }
}

void helper_class::set_capa(float capa) {
    capa_ = capa;
}

auto helper_class::print_choices(const std::vector<t_cho>& choices) -> std::string {

    if (choices.empty()) {
        return "< >";
    }
    std::string ret = "<";


    for (unsigned int i = 0; i < choices.size() - 1; i++) {
        ret += std::to_string(choices.at(i).cho_obj_nr) + ":" + std::to_string(static_cast <int> (choices.at(i).cho_factor)) + ", ";
    }

    ret += std::to_string(choices.at(choices.size() - 1).cho_obj_nr) + ":" + std::to_string(static_cast <int> (choices.at(choices.size() - 1).cho_factor)) + ">";

    return ret;

}

auto helper_class::get_opt_sol() -> std::vector<t_cho> {
    return opt_sol_;
}

auto helper_class::get_branch_count() -> unsigned int {
    return branch_count_;
}

void helper_class::generate_random_config(unsigned int min_amount, unsigned int max_amount) {
    auto rand_amount = static_cast <unsigned int> (min_amount + (rand() % (max_amount - min_amount - 1)));
    branch_amount_ = pow(2, rand_amount + 1 ) - 2;
    float total_weight = 0;

    // set random objects
    objects_.clear();
    objects_.reserve(rand_amount);
    for (unsigned int i = 0; i < rand_amount; i++) {
        t_obj temp{};
        temp.obj_nr = i;
        temp.obj_value = rand() % 10000 + 1;
        temp.obj_weight = rand() % 6 + 1;
        temp.obj_ratio = temp.obj_value / temp.obj_weight;
        objects_.push_back(temp);

        total_weight += temp.obj_weight;
    }

    // set capacity between 65% and 85% of the total weight
    auto rand_capa_factor = static_cast<float>(650 + (rand() % (200))) / 1000;
    capa_ = rand_capa_factor * total_weight;
}

void helper_class::reset_algo() {
    objects_.clear();
    opt_sol_.clear();
    opt_win_ = 0;
    branch_count_ = 0;
}

unsigned int helper_class::get_branch_amount() {
    return branch_amount_;
}

