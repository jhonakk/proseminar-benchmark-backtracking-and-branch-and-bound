#ifndef KNAPSACK_LIFO_HPP
#define KNAPSACK_LIFO_HPP

#include "helper_class.hpp"

class lifo : public helper_class {

public:
    // constructor
    lifo(float capa, const std::vector<t_obj>& objects);
    lifo();
    ~lifo();

    // algorithm
    void run_algorithm();
    void algo_lifo(unsigned int depth, std::vector<t_cho> choices, float value, float weight);
    void algo_lifo_branch(unsigned int depth, std::vector<t_cho> choices, float value, float weight);
};

#endif //KNAPSACK_LIFO_HPP
