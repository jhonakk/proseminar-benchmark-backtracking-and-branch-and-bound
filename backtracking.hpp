#ifndef KNAPSACK_BACKTRACKING_HPP
#define KNAPSACK_BACKTRACKING_HPP

#include "helper_class.hpp"

class backtracking : public helper_class {

public:
    // constructor
    backtracking(float capa, const std::vector<t_obj>& objects);
    backtracking();
    ~backtracking();

    // algorithm
    void run_algorithm();
    void algo_backtracking(unsigned int depth, std::vector<t_cho>);
};

#endif //KNAPSACK_BACKTRACKING_HPP
