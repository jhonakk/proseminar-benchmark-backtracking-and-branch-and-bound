#include <cmath>
#include "backtracking.hpp"

backtracking::backtracking(float capa, const std::vector<t_obj>& objects) {
    capa_ = capa;
    objects_ = objects;
    opt_sol_ = {};
    opt_win_ = 0;
    branch_count_ = 0;
    branch_amount_ = pow(2, objects_.size() + 1 ) - 2;
}

backtracking::backtracking() {
    opt_sol_ = {};
    opt_win_ = 0;
    branch_count_ = 0;
    branch_amount_ = pow(2, objects_.size() + 1 ) - 2;
    generate_random_config(DEFAULT_MIN, DEFAULT_MAX);
}

backtracking::~backtracking() = default;

void backtracking::run_algorithm() {

    std::vector<t_cho> choices;
    algo_backtracking(0, choices);
}

void backtracking::algo_backtracking(unsigned int depth, std::vector<t_cho> choices) {

    if (depth < objects_.size()) {
        // generate zero-choice
        t_cho new_choice{};
        new_choice.cho_obj_nr = depth;
        new_choice.cho_chosen = true;

        new_choice.cho_factor = 0;
        choices.push_back(new_choice);
        branch_count_++;
        algo_backtracking(depth + 1, choices);

        choices.pop_back();
        branch_count_--;

        // generate one-choice
        new_choice.cho_factor = 1;
        choices.push_back(new_choice);

        if (weight(choices) <= capa_) {

            branch_count_++;
            algo_backtracking(depth + 1, choices);
        }
    } else {
        if (opt_win_ < value(choices)) {
            opt_win_ = value(choices);
            opt_sol_ = choices;
        }
    }
}

