#include <iostream>
#include "statistics.hpp"

statistics::statistics(unsigned int runs, unsigned int algo_amount) {
    runs_ = runs;
    algo_amount_ = algo_amount;

    for (unsigned int i = 0; i < algo_amount_; i++) {
        data_.push_back(std::vector<float>());
    }
}

statistics::~statistics() = default;

void statistics::set_data(unsigned int algo, float data) {
    data_.at(algo).push_back(data);
}

auto statistics::get_data() -> std::vector<std::vector<float>> {
    return data_;
}

void statistics::print_results() {
    for (unsigned int i = 0; i < algo_amount_; i++) {
        for (auto val : data_.at(i)) {
            std::cout << val << ", ";
        }
        std::cout << std::endl;
    }
}

auto statistics::mean_value() -> std::vector<float> {
    std::vector<float> results = {};
    float r = 0;

    for (unsigned int i = 0; i < algo_amount_; i++) {
        r = 0;
        for (auto val : data_.at(i)) {
            r += val;
        }
        results.push_back(r / data_.at(i).size());
    }

    return results;
}

auto statistics::variance() -> std::vector<float> {
    std::vector<float> results = {};
    float r = 0;

    for (unsigned int i = 0; i < algo_amount_; i++) {
        r = 0;
        for (auto val : data_.at(i)) {
            r += (val - mean_value().at(i)) * (val - mean_value().at(i));
        }
        results.push_back(r / (data_.at(i).size() - 1));
    }

    return results;
}