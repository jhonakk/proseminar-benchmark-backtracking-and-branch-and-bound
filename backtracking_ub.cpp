#include <cmath>
#include "backtracking_ub.hpp"

backtracking_ub::backtracking_ub(float capa, const std::vector<t_obj>& objects) {
    capa_ = capa;
    objects_ = objects;
    opt_sol_ = {};
    opt_win_ = 0;
    branch_count_ = 0;
    branch_amount_ = pow(2, objects_.size() + 1 ) - 2;
}

backtracking_ub::backtracking_ub() {
    opt_sol_ = {};
    opt_win_ = 0;
    branch_count_ = 0;
    branch_amount_ = pow(2, objects_.size() + 1 ) - 2;
    generate_random_config(DEFAULT_MIN, DEFAULT_MAX);
}

backtracking_ub::~backtracking_ub() = default;

void backtracking_ub::run_algorithm() {

    std::vector<t_cho> choices;
    algo_backtracking_ub(0, choices, 0, 0);
}

void backtracking_ub::algo_backtracking_ub(unsigned int depth, std::vector<t_cho> choices, float value, float weight) {

    float ub = calc_ub(choices);

    if (depth < objects_.size()) {
        if (ub > opt_win_) {
            t_cho new_choice{};
            new_choice.cho_obj_nr = depth;
            new_choice.cho_chosen = true;

            // generate one-choice
            if (weight + objects_.at(depth).obj_weight <= capa_) {
                new_choice.cho_factor = 1;
                choices.push_back(new_choice);
                branch_count_++;
                algo_backtracking_ub(depth + 1, choices, value + objects_.at(depth).obj_value, weight + objects_.at(depth).obj_weight);

                choices.pop_back();
                branch_count_--;
            }

            // generate zero-choice
            new_choice.cho_factor = 0;
            choices.push_back(new_choice);

            branch_count_++;
            algo_backtracking_ub(depth + 1, choices, value, weight);
        }
    } else {
        if (opt_win_ < value) {
            opt_win_ = value;
            opt_sol_ = choices;
        }
    }
}