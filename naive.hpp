#ifndef KNAPSACK_NAIVE_HPP
#define KNAPSACK_NAIVE_HPP

#include "helper_class.hpp"

class naive : public helper_class {

public:
    // constructor
    naive(float capa, const std::vector<t_obj>& objects);
    naive();
    ~naive();

    // algorithm
    void run_algorithm();
    void algo_naive(unsigned int depth, std::vector<bool> choices);
};

#endif //KNAPSACK_NAIVE_HPP
