#include <cmath>
#include "lifo.hpp"

lifo::lifo(float capa, const std::vector<t_obj>& objects) {
    capa_ = capa;
    objects_ = objects;
    opt_sol_ = {};
    opt_win_ = 0;
    branch_count_ = 0;
    branch_amount_ = pow(2, objects_.size() + 1 ) - 2;
}

lifo::lifo() {
    opt_sol_ = {};
    opt_win_ = 0;
    branch_count_ = 0;
    generate_random_config(DEFAULT_MIN, DEFAULT_MAX);
}

lifo::~lifo() = default;

void lifo::run_algorithm() {

    std::vector<t_cho> choices;
    algo_lifo(0, choices, 0, 0);
}

void lifo::algo_lifo(unsigned int depth, std::vector<t_cho> choices, float value, float weight) {

    float ub = calc_ub(choices);

    if (depth < objects_.size()) {
        if (ub > opt_win_) {
            algo_lifo_branch(depth, choices, value, weight);
        }
    } else {
        if (opt_win_ < value) {
            opt_win_ = value;
            opt_sol_ = choices;
        }
    }
}

void lifo::algo_lifo_branch(unsigned int depth, std::vector<t_cho> choices, float value, float weight) {

    t_cho new_choice{};

    // generate zero_choice
    std::vector<t_cho> zero_choice = choices; // need init from choices
    new_choice.cho_obj_nr = depth;
    new_choice.cho_factor = 0;
    zero_choice.push_back(new_choice);

    // generate one_choice
    new_choice.cho_factor = 1;
    choices.push_back(new_choice);

    if (weight + objects_.at(depth).obj_weight <= capa_) {
        // choose successor with better ub first:
        if (calc_ub(choices) > calc_ub(zero_choice)) {
            branch_count_++;
            algo_lifo(depth + 1, choices, value + objects_.at(depth).obj_value, weight + objects_.at(depth).obj_weight); // one-choice
            branch_count_++;
            algo_lifo(depth + 1, zero_choice, value, weight); // zero-choice // dont we need to use the zero choice vector here 
        } else {
            branch_count_++;
            algo_lifo(depth + 1, zero_choice, value, weight); // zero-choice // dont we need to use the zero choice vector here 
            branch_count_++;
            algo_lifo(depth + 1, choices, value + objects_.at(depth).obj_value, weight + objects_.at(depth).obj_weight); // one-choice
        }
    } else {
        // only consider zero-choice
        branch_count_++;
        algo_lifo(depth + 1, zero_choice, value, weight); // dont we need to use the zero choice vector here 
    }
}