#ifndef KNAPSACK_HELPER_CLASS_HPP
#define KNAPSACK_HELPER_CLASS_HPP

#include <vector>
#include <string>

#define DEFAULT_MIN 4
#define DEFAULT_MAX 20

struct t_obj {
    int obj_nr;
    float obj_value;
    float obj_weight;
    float obj_ratio;
};

struct t_cho {
    int cho_obj_nr;
    bool cho_chosen = false;
    float cho_factor = 0;
};

inline bool operator< (const t_cho& t1, const t_cho& t2) {
    return (t1.cho_obj_nr > t2.cho_obj_nr);
}

class helper_class {

public:
    static auto print_choices(const std::vector<t_cho>& choices) -> std::string;
    auto get_opt_sol() -> std::vector<t_cho>;
    auto get_branch_count() -> unsigned int;
    auto get_branch_amount() -> unsigned int;
    void generate_random_config(unsigned int min_amount, unsigned int max_amount);
    virtual void run_algorithm() = 0;
    void reset_algo();

protected:

    float capa_;
    unsigned int branch_count_;
    unsigned int branch_amount_;
    float opt_win_;
    std::vector<t_cho> opt_sol_;
    std::vector<t_obj> objects_;

    float value(const std::vector<struct t_cho>& choices);
    float weight(const std::vector<struct t_cho>& choices);
    float calc_ub(const std::vector<struct t_cho>& choices);
    void init_vector_of_choices(std::vector<struct t_cho>& choices, const std::vector<struct t_obj>& objects);
    void set_capa(float capa);
};

#endif //KNAPSACK_HELPER_CLASS_HPP