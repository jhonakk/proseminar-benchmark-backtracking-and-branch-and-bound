#ifndef STATISTICS_HPP
#define STATISTICS_HPP

#include <vector>
#include "helper_class.hpp"

class statistics {

public:
    // constructor
    statistics(unsigned int runs, unsigned int algo_amount);
    ~statistics();

    //methods
    void set_data(unsigned int algo, float data);
    auto get_data() -> std::vector<std::vector<float>>;
    void print_results();
    auto mean_value() -> std::vector<float>;
    auto variance() -> std::vector<float>;

private:
    unsigned int runs_;
    unsigned int algo_amount_;
    std::vector<std::vector<float>> data_;
};

#endif //STATISTICS_HPP
